package sp.repcount

import android.app.Application


/**
 * This is just here for the sample to illustrate usage. DI is anoher discussion ;)
 */
lateinit var appContainer:AppContainer

class SampleApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        appContainer = AppContainer(this)
    }
}