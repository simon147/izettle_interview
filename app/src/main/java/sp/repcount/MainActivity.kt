package sp.repcount.services.analytics

import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import sp.repcount.R
import sp.repcount.appContainer
import sp.repcount.services.analytics.core.AnalyticsEvent
import sp.repcount.services.analytics.core.AnalyticsUserProperty
import sp.repcount.services.analytics.core.CustomerType

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))


        val analyticsManager = appContainer.analyticsManager

        analyticsManager.identify("myUserId", "sample@email.com")
        analyticsManager.setUserProperty(AnalyticsUserProperty.AppVersion())
        analyticsManager.setUserProperty(AnalyticsUserProperty.CustomerType(CustomerType.SUBSCRIBER))


        findViewById<FloatingActionButton>(R.id.fab).setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }
    }
}