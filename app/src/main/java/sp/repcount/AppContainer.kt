package sp.repcount

import android.app.Application
import android.content.Context
import com.amplitude.api.Amplitude
import com.google.firebase.analytics.FirebaseAnalytics
import sp.repcount.services.analytics.core.AnalyticsEngine
import sp.repcount.services.analytics.core.AnalyticsManager
import sp.repcount.services.analytics.engines.AmplitudeAnalyticsEngine
import sp.repcount.services.analytics.engines.DebugAnalyticsEngine
import sp.repcount.services.analytics.engines.FirebaseAnalyticsEngine

class AppContainer(private val application: Application) {

    final val analyticsManager:AnalyticsManager by lazy { createAnalyticsManager() }
    private fun createAnalyticsManager(): AnalyticsManager {
        val analyticsEngines = mutableListOf<AnalyticsEngine>()
        if(BuildConfig.DEBUG){
            //Add your debug engines here
            analyticsEngines.add(DebugAnalyticsEngine())
        }

        //analyticsEngines.add(AmplitudeAnalyticsEngine(application, Amplitude.getInstance()))
        //analyticsEngines.add(FirebaseAnalyticsEngine(application, FirebaseAnalytics.getInstance(context)))
        return AnalyticsManager(analyticsEngines)
    }

}