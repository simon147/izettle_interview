package sp.repcount.services.analytics.engines

import android.os.Bundle
import com.helpscout.beacon.Beacon
import sp.repcount.services.analytics.core.AnalyticsEngine
import sp.repcount.services.analytics.core.AnalyticsPurpose
import java.util.*

class HelpScoutAnalyticsEngine(): AnalyticsEngine {

    override fun sendAnalyticsEvent(eventName: String, metadata: Bundle?, purposes: Set<AnalyticsPurpose>) {
        //Not used in helpscout
    }

    override fun identify(uid: String?, email: String?) {

        if(email != null) {
            Beacon.identify(email, null)
        } else {
            Beacon.logout()
        }
    }

    override fun setUserProperty(propertyName: String, value: Any?, purposes: Set<AnalyticsPurpose>) {
        value?.toString()?.let { propertyValue ->
            Beacon.addAttributeWithKey(propertyName.toLowerCase(Locale.ENGLISH).replace(" ", "-"), propertyValue)
        }
    }

    override val purposes: Set<AnalyticsPurpose>
        get() = setOf(AnalyticsPurpose.SUPPORT)



}