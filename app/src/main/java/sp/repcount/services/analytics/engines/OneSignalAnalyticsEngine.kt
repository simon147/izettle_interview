package sp.repcount.services.analytics.engines

import android.os.Bundle
import com.onesignal.OneSignal
import sp.repcount.services.analytics.core.AnalyticsEngine
import sp.repcount.services.analytics.core.AnalyticsPurpose

class OneSignalAnalyticsEngine() :AnalyticsEngine{


    override fun setUserProperty(
        propertyName: String,
        value: Any?,
        purposes: Set<AnalyticsPurpose>
    ) {

        when(value) {
            null -> OneSignal.deleteTag(propertyName)
            else -> OneSignal.sendTag(propertyName, value.toString())
        }
    }
    override fun sendAnalyticsEvent(
        eventName: String,
        metadata: Bundle?,
        purposes: Set<AnalyticsPurpose>
    ) {
        OneSignal.sendTag(eventName, "${System.currentTimeMillis()}")
    }
    override fun identify(uid: String?, email: String?) {
        uid?.let {
            OneSignal.setExternalUserId(it)
        }
    }
    override val purposes: Set<AnalyticsPurpose>
        get() = setOf(AnalyticsPurpose.MESSAGING)
}