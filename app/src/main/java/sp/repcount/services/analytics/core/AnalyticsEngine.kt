package sp.repcount.services.analytics.core

import android.os.Bundle

interface AnalyticsEngine {
    fun setUserProperty(propertyName: String, value:Any?,  purposes:Set<AnalyticsPurpose>)
    fun sendAnalyticsEvent(eventName: String, metadata: Bundle?, purposes: Set<AnalyticsPurpose>)
    fun identify(uid: String?, email:String?)
    val purposes:Set<AnalyticsPurpose>
}