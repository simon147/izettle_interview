package sp.repcount.services.analytics.core

import sp.repcount.BuildConfig
import java.sql.Date
import java.util.*

/**
 * Keep this in sync with AnalyticsUserProperty.swift
 */

enum class CustomerType {
    FREE, PAID, INAPP, SUBSCRIBER;

    fun getName():String {
        return when(this){
            FREE -> "Free"
            PAID -> "Paid"
            INAPP -> "In App Purchase"
            SUBSCRIBER -> "Subscription"
        }
    }
}
sealed class AnalyticsUserProperty(
    val name: String,
    val purposes: Set<AnalyticsPurpose>,
    val params: Any?
) {

    class AppVersion : AnalyticsUserProperty("App Version", setOf(AnalyticsPurpose.SUPPORT),"${BuildConfig.VERSION_NAME}(${BuildConfig.VERSION_CODE})")
    class CustomerType(customerType: sp.repcount.services.analytics.core.CustomerType) : AnalyticsUserProperty("Customer Type", setOf(AnalyticsPurpose.SUPPORT, AnalyticsPurpose.MESSAGING),customerType.getName())
}