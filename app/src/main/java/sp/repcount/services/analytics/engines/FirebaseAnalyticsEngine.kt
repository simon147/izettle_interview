package sp.repcount.services.analytics.engines

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import sp.repcount.services.analytics.core.AnalyticsEngine
import sp.repcount.services.analytics.core.AnalyticsPurpose
import java.util.*

class FirebaseAnalyticsEngine(val context:Context, val analytics: FirebaseAnalytics): AnalyticsEngine {

    override fun setUserProperty(propertyName: String, value: Any?, purposes:Set<AnalyticsPurpose>) {
        analytics.setUserProperty(adaptToFirebase(propertyName), value.toString())
    }

    override fun sendAnalyticsEvent(eventName: String, metadata:Bundle?, purposes: Set<AnalyticsPurpose>) {
        analytics.logEvent(adaptToFirebase(eventName), metadata)
    }

    override fun identify(uid: String?, email: String?) {
        analytics.setUserId(uid)
    }

    private fun adaptToFirebase(name:String):String{
        return name.replace(" ", "_").toLowerCase(Locale.ENGLISH).take(40)
    }

    override val purposes: Set<AnalyticsPurpose>
        get() = setOf(AnalyticsPurpose.ANALYTICS, AnalyticsPurpose.DIAGNOSTICS, AnalyticsPurpose.MESSAGING)
}