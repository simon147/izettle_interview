package sp.repcount.services.analytics.engines

import android.content.Context
import android.os.Bundle
import com.amplitude.api.AmplitudeClient
import com.amplitude.api.Identify
import org.json.JSONException
import org.json.JSONObject
import sp.repcount.services.analytics.core.AnalyticsEngine
import sp.repcount.services.analytics.core.AnalyticsPurpose

class AmplitudeAnalyticsEngine(val context:Context, val amplitude:AmplitudeClient): AnalyticsEngine {

    override fun sendAnalyticsEvent(eventName: String, metadata: Bundle?, purposes:Set<AnalyticsPurpose>) {
        amplitude.logEvent(eventName, bundleToJson(metadata))
    }

    override fun identify(uid: String?, email: String?) {
        uid?.let {
            amplitude.setUserId(it, false)
        }
    }

    override fun setUserProperty(propertyName: String, value: Any?, purposes:Set<AnalyticsPurpose>) {

        value?.let { propertyValue ->
            val identity = Identify().set(propertyName, propertyValue.toString())
            amplitude.identify(identity)
        }
    }

    override val purposes: Set<AnalyticsPurpose>
        get() = setOf(AnalyticsPurpose.ANALYTICS)

    private fun bundleToJson(bundle:Bundle?):JSONObject? {
        if(bundle == null){
            return null
        }
        val json = JSONObject()
        val keys: Set<String> = bundle.keySet()
        for (key in keys) {
            try {
                json.put(key, JSONObject.wrap(bundle.get(key)))
            } catch (e: JSONException) {
                //Handle exception here
            }
        }
        return json
    }
}