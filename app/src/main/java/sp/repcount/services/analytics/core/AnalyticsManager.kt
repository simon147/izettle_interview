package sp.repcount.services.analytics.core

import android.content.Context
class AnalyticsManager(val engines:List<AnalyticsEngine>) {

    fun log(event: AnalyticsEvent) {
        engines.forEach { engine ->
            if (shouldTrackEvent(event, engine)){
                engine.sendAnalyticsEvent(event.name, event.params, event.purposes)
            }
        }
    }

    final fun setUserProperty(property: AnalyticsUserProperty){
        engines.forEach { engine ->
            if (shouldTrackUserProperty(property, engine)){
                engine.setUserProperty(property.name, property.params, property.purposes)
            }
        }
    }

    fun identify(userId:String?, email:String?){
        engines.forEach { engine ->
            engine.identify(userId, email)
        }
    }

    private fun shouldTrackUserProperty(property:AnalyticsUserProperty, engine:AnalyticsEngine):Boolean{
        val propertyPurposes = property.purposes
        val enginePurposes = engine.purposes
        return propertyPurposes.intersect(enginePurposes).isNotEmpty()
    }

    private fun shouldTrackEvent(event:AnalyticsEvent, engine:AnalyticsEngine):Boolean{
        val eventPurposes:Set<AnalyticsPurpose> = event.purposes
        val enginePurposes:Set<AnalyticsPurpose> = engine.purposes
        return eventPurposes.intersect(enginePurposes).isNotEmpty()
    }
}
