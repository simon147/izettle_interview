package sp.repcount.services.analytics.core

import android.os.Bundle
import androidx.core.os.bundleOf

enum class AnalyticsPurpose {
    ANALYTICS, SUPPORT, MESSAGING, DIAGNOSTICS
}
sealed class AnalyticsEvent(
    val name: String,
    val purposes: Set<AnalyticsPurpose>,
    val params: Bundle?
) {
    class AnAnalyticsEvent(val atTime:Long):AnalyticsEvent("First Button Click", setOf(AnalyticsPurpose.ANALYTICS),
        bundleOf("time" to atTime))
    class AnEventThatTriggersAMessage(val somethingWeShouldSend:String):AnalyticsEvent("Message Trigger", setOf(AnalyticsPurpose.MESSAGING),
        bundleOf("somethingToSend" to somethingWeShouldSend))

}