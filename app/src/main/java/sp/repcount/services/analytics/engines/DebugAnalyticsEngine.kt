package sp.repcount.services.analytics.engines
import android.os.Bundle
import android.util.Log
import sp.repcount.services.analytics.core.AnalyticsEngine
import sp.repcount.services.analytics.core.AnalyticsPurpose

class DebugAnalyticsEngine: AnalyticsEngine {

    override fun sendAnalyticsEvent(eventName: String, metadata: Bundle?, purposes:Set<AnalyticsPurpose>) {
        Log.d(TAG, "send analytics event $eventName to ${describePurposes(purposes)}")

        metadata?.let {
            Log.d(TAG, "params" )
            for (key in it.keySet()) {
                Log.d(TAG, "key $key value ${it.get(key)}")
            }
        }
    }

    private fun describePurposes(purposes: Set<AnalyticsPurpose>):String{
        return purposes.joinToString(",") { it.name }
    }

    override fun setUserProperty(propertyName: String, value: Any?, purposes: Set<AnalyticsPurpose>) {
        value?.let {
            Log.d(TAG, "set user property: $propertyName to $it for :${describePurposes(purposes)}")
        }
    }


    override fun identify(uid: String?, email: String?) {
        if(uid != null){
            Log.d(TAG, "setting user identifier to ${uid}")
        } else {
            Log.d(TAG, "Clearing user")
        }
    }

    override val purposes: Set<AnalyticsPurpose>
        get() = setOf(AnalyticsPurpose.MESSAGING, AnalyticsPurpose.DIAGNOSTICS, AnalyticsPurpose.SUPPORT, AnalyticsPurpose.MESSAGING, AnalyticsPurpose.ANALYTICS)

    companion object {
        const val TAG = "DEBUG_ANALYTICS_ENGINE"
    }
}